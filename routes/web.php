<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



//Route::resource('admin/posts', 'Admin\\PostsController');

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/form', 'HomeController@form');
    Route::get('/crudform', 'HomeController@crudform');
    Route::resource('settings/department', 'Settings\\DepartmentController');
});


Auth::routes();