<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <html> <![endif]-->
<!--[if !IE]><!--><html><!-- <![endif]-->
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Meta -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />

    <!--
    **********************************************************
    In development, use the LESS files and the less.js compiler
    instead of the minified CSS loaded by default.
    **********************************************************
    <link rel="stylesheet/less" href="../assets/less/admin/module.admin.page.support_forum_post.less" />
    -->

    <!--[if lt IE 9]> {!! Html::style('public/assets/components/library/bootstrap/css/bootstrap.min.css') !!}<![endif]-->
{!! Html::style('public/assets/css/admin/module.admin.page.form_elements.min.css') !!}
{!! Html::style('public/assets/css/style.css') !!}
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    {!! Html::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') !!}
    {!! Html::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') !!}
    <![endif]-->


    {!! Html::script('public/assets/components/library/jquery/jquery.min.js?v=v1.2.3') !!}
    {!! Html::script('public/assets/components/library/jquery/jquery-migrate.min.js?v=v1.2.3') !!}
    {!! Html::script('public/assets/components/library/modernizr/modernizr.js?v=v1.2.3') !!}
    {!! Html::script('public/assets/components/plugins/less-js/less.min.js?v=v1.2.3') !!}
    {!! Html::script('public/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.2.3') !!}
    {!! Html::script('public/assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.2.3') !!}
</head>
<body class="">

{{--Header--}}
@include('common.header')

{{--Sidebar--}}
@include('common.sidebar')

<div id="content">
    {{--Content--}}
    @yield('content')

    {{--Footer--}}
    @include('common.footer')
</div>



<!-- Global -->
<script>
    var basePath = '',
            commonPath = '../assets/',
            rootPath = '../',
            DEV = false,
            componentsPath = '../assets/components/';

    var primaryColor = '#81A594',
            dangerColor = '#b55151',
            infoColor = '#466baf',
            successColor = '#8baf46',
            warningColor = '#ab7a4b',
            inverseColor = '#45484d';

    var themerPrimaryColor = primaryColor;
</script>

{!! Html::script('public/assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/plugins/breakpoints/breakpoints.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/core/js/animations.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/plugins/holder/holder.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/core/js/sidebar.main.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/core/js/sidebar.collapse.init.js?v=v1.2.3') !!}

{!! Html::script('public/assets/components/modules/admin/forms/elements/bootstrap-switch/assets/lib/js/bootstrap-switch.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/bootstrap-switch/assets/custom/js/bootstrap-switch.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/uniform/assets/lib/js/jquery.uniform.min.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/uniform/assets/custom/js/uniform.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/fuelux-checkbox/fuelux-checkbox.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/modals/assets/js/bootbox.min.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/modals/assets/js/modals.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/notifications/gritter/assets/lib/js/jquery.gritter.min.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/notifications/gritter/assets/custom/js/gritter.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/editors/wysihtml5/assets/lib/js/wysihtml5-0.3.0_rc2.min.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/editors/wysihtml5/assets/lib/js/bootstrap-wysihtml5-0.0.2.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/editors/wysihtml5/assets/custom/wysihtml5.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/wizards/assets/lib/jquery.bootstrap.wizard.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/wizards/assets/custom/js/form-wizards.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/jasny-fileupload/assets/js/bootstrap-fileupload.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/button-states/button-loading/assets/js/button-loading.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/select2/assets/lib/js/select2.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/select2/assets/custom/js/select2.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/inputmask/assets/lib/jquery.inputmask.bundle.min.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/inputmask/assets/custom/inputmask.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/bootstrap-datepicker/assets/lib/js/bootstrap-datepicker.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/bootstrap-datepicker/assets/custom/js/bootstrap-datepicker.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/bootstrap-timepicker/assets/lib/js/bootstrap-timepicker.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/bootstrap-timepicker/assets/custom/js/bootstrap-timepicker.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/colorpicker-farbtastic/assets/js/farbtastic.min.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/modules/admin/forms/elements/colorpicker-farbtastic/assets/js/colorpicker-farbtastic.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/plugins/holder/holder.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/core/js/sidebar.main.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/core/js/sidebar.collapse.init.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/helpers/themer/assets/plugins/cookie/jquery.cookie.js?v=v1.2.3') !!}
{!! Html::script('public/assets/components/core/js/core.init.js?v=v1.2.3') !!}
</body>
</html>