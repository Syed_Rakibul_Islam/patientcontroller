@extends('layouts.form')

@section('content')




    <div class="innerAll spacing-x2">
        <h3 class="heading">Horizontal Form</h3>
        <hr class="custom"/>
        <!-- Column -->
        <div class="col-md-8 col-md-offset-2">
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-2 control-label">User</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">mosaicpro</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label class="checkbox-custom">
                                <i class="fa fa-fw fa-square-o"></i>
                                <input type="checkbox" checked="checked"> Remember me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Sign in</button>
                    </div>
                </div>
            </form>


        </div>
        <!-- // Column END -->

    </div>


@endsection
