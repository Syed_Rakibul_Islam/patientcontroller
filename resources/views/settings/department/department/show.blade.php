@extends('layouts.app')

@section('content')
    <div class="innerAll spacing-x2">
        <div class="panel panel-default">
            <div class="panel-heading">Department {{ $department->id }}</div>
            <div class="panel-body">

                <a href="{{ url('settings/department/' . $department->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Department"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['settings/department', $department->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Department',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                {!! Form::close() !!}
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $department->id }}</td>
                            </tr>
                            <tr><th> Name </th><td> {{ $department->name }} </td></tr><tr><th> Description </th><td> {{ $department->description }} </td></tr><tr><th> Amount </th><td> {{ $department->amount }} </td></tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection