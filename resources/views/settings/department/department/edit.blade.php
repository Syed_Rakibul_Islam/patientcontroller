@extends('layouts.form')

@section('content')
    <div class="innerAll spacing-x2">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Department {{ $department->id }}</div>
            <div class="panel-body">

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($department, [
                    'method' => 'PATCH',
                    'url' => ['/settings/department', $department->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}

                @include ('settings/department.department.form', ['submitButtonText' => 'Update'])

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection