@extends('layouts.form')

@section('content')
    <div class="innerAll spacing-x2">
        <div class="panel panel-default">
            <div class="panel-heading">Create New Department</div>
            <div class="panel-body">
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::open(['url' => '/settings/department', 'class' => 'form-horizontal', 'files' => true]) !!}

                @include ('settings/department.department.form')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection