@extends('layouts.app')

@section('content')
    <div class="innerAll spacing-x2">
        <div class="panel panel-default">
            <div class="panel-heading">Department</div>
            <div class="panel-body">

                <a href="{{ url('/settings/department/create') }}" class="btn btn-primary btn-xs" title="Add New Department"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                <br/>
                <br/>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>ID</th><th> Name </th><th> Description </th><th> Amount </th><th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($department as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td><td>{{ $item->description }}</td><td>{{ $item->amount }}</td>
                                <td>
                                    <a href="{{ url('/settings/department/' . $item->id) }}" class="btn btn-success btn-xs" title="View Department"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                    <a href="{{ url('/settings/department/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Department"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/settings/department', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Department" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete Department',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper"> {!! $department->render() !!} </div>
                </div>

            </div>
        </div>
    </div>
@endsection