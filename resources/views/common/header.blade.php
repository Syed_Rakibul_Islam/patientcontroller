<div class="navbar navbar-fixed-top navbar-primary main" role="navigation">

    <div class="navbar-header pull-left">
        <div class="navbar-brand">
            <div class="pull-left">
                <a href="" class="toggle-button toggle-sidebar btn-navbar"><i class="fa fa-bars"></i></a>
            </div>
            <a href="index.html?lang=en" class="appbrand innerL">Patient Comptroller</a>
        </div>
    </div>

    <ul class="nav navbar-nav navbar-left">
        <li class=" hidden-xs">
            <form class="navbar-form navbar-left " role="search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Type in here..."/>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-inverse"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>
        </li>
        <li class="dropdown">
            <a href="" class="dropdown-toggle user" data-toggle="dropdown"> <img src="{{ url('public/assets/images/people/35/8.jpg') }}" alt="" class="img-circle"/><span class="hidden-xs hidden-sm"> &nbsp; Adrian Demian </span> <span class="caret"></span></a>
            <ul class="dropdown-menu list pull-right ">
                <li><a href="#">Your Profile <i class="fa fa-user pull-right"></i></a></li>
                <li><a href="#">Edit Account <i class="fa fa-pencil pull-right"></i></a></li>
                <li><a href="#">Get Help <i class="fa fa-question-circle pull-right"></i></a></li>
                <li><a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();
                                        ">Log out <i class="fa fa-sign-out pull-right"></i></a></li>
            </ul>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right hidden-xs">
        {{--<li class="dropdown notification">--}}
            {{--<a href="#" class="dropdown-toggle menu-icon" data-toggle="dropdown"><i class="fa fa-fw fa-envelope-o"></i><span class="badge badge-primary">2</span></a>--}}
            {{--<ul class="dropdown-menu inbox">--}}
                {{--<li class="headline">Inbox</li>--}}
                {{--<li><input type="text" class="form-control border-none" placeholder="Search Email..." /></li>--}}
                {{--<li>--}}
                    {{--<div class="media">--}}
                        {{--<a class="pull-left" href="#"><img src="../assets/images/people/50/8.jpg" alt="" class="img-circle media-object"></a>--}}
                        {{--<div class="media-body">--}}
                            {{--<a href="" class="strong text-primary"> <i class="fa fa-circle"></i> Adrian Demian</a> <span>(3)</span><span class="pull-right time-email">10:20 AM</span>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--Lorem ipsum dolor sit amet, consecte adipisicing elit.--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="media">--}}
                        {{--<a class="pull-left" href="#"><img src="../assets/images/people/50/3.jpg" alt="" class="img-circle media-object"></a>--}}
                        {{--<div class="media-body">--}}
                            {{--<a href="" class="strong text-primary"> Joanne Smith</a><span>(1)</span><span class="pull-right time-email">06:48 PM</span>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--Lorem ipsum dolor sit amet, consecte adipisicing elit.--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="media">--}}
                        {{--<a class="pull-left" href="#"><img src="../assets/images/people/50/20.jpg" alt="" class="img-circle media-object"></a>--}}
                        {{--<div class="media-body">--}}
                            {{--<a href="" class="strong text-primary"> <i class="fa fa-circle"></i> Mister Awesome</a> <span>(7)</span><span class="pull-right time-email">04:22 PM</span>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--Lorem ipsum dolor sit amet, consecte adipisicing elit.--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        {{--<li class="dropdown notification hidden-sm hidden-md">--}}
            {{--<a href="#" class="dropdown-toggle menu-icon" data-toggle="dropdown"><i class="fa fa-fw fa-exclamation-circle"></i><span class="badge badge-info">2</span></a>--}}
            {{--<ul class="dropdown-menu">--}}
                {{--<li><a href="#">Action</a></li>--}}
                {{--<li><a href="#">Another action</a></li>--}}
                {{--<li><a href="#">Something else here</a></li>--}}
                {{--<li><a href="#">Separated link</a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        {{--<li class="dropdown notification hidden-sm hidden-md">--}}
            {{--<a href="#" class="dropdown-toggle menu-icon" data-toggle="dropdown"><i class="fa fa-fw fa-dropbox"></i><span class="badge badge-success badge-icon"><i class="fa fa-check"></i></span></a>--}}
            {{--<ul class="dropdown-menu dropbox list">--}}
                {{--<li class="headline">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-6">Dropbox Folder</div>--}}
                        {{--<div class="col-md-6">--}}
                            {{--<a href="">Settings <i class="fa fa-cog"></i></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li class="clean info">RECENTLY CHANGED</li>--}}
                {{--<li>--}}
                    {{--<ul class="media-list">--}}
                        {{--<li class="media innerAll half">--}}
                            {{--<a class="pull-left" href="#"><img class="media-object" data-src="holder.js/35x35" alt="..."></a>--}}
                            {{--<div class="media-body margin-none">--}}
                                {{--<a class="padding-none">Media heading</a>--}}
                                {{--<div class="clearfix"></div>--}}
                                {{--<em>59 minutes ago</em>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<ul class="media-list">--}}
                        {{--<li class="media innerAll half">--}}
                            {{--<a class="pull-left" href="#"><img class="media-object" data-src="holder.js/35x35" alt="..."></a>--}}
                            {{--<div class="media-body margin-none">--}}
                                {{--<a class="padding-none">Media heading</a>--}}
                                {{--<div class="clearfix"></div>--}}
                                {{--<em>59 minutes ago</em>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<ul class="media-list">--}}
                        {{--<li class="media innerAll half">--}}
                            {{--<a class="pull-left" href="#"><img class="media-object" data-src="holder.js/35x35" alt="..."></a>--}}
                            {{--<div class="media-body margin-none">--}}
                                {{--<a class="padding-none">Media heading</a>--}}
                                {{--<div class="clearfix"></div>--}}
                                {{--<em>59 minutes ago</em>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<ul class="media-list">--}}
                        {{--<li class="media innerAll half">--}}
                            {{--<a class="pull-left" href="#"><img class="media-object" data-src="holder.js/35x35" alt="..."></a>--}}
                            {{--<div class="media-body margin-none">--}}
                                {{--<a class="padding-none">Media heading</a>--}}
                                {{--<div class="clearfix"></div>--}}
                                {{--<em>59 minutes ago</em>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        {{--<li class="dropdown">--}}
            {{--<a href="" class="dropdown-toggle" data-toggle="dropdown"><img src="../assets//images/lang/en-flat.jpg" /></a>--}}
            {{--<ul class="dropdown-menu pull-right">--}}
                {{--<li class="active"><a href="">English</a></li>--}}
                {{--<li><a href="">Romanian</a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        <li><a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();
                                                    " class="menu-icon"><i class="fa fa-sign-out"></i></a>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</div>